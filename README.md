# MySQL Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Installed packages ###
* mysql-server

### Specifications ###
* forwarded port: 3306
* volumes: `/etc/mysql`, `/var/lib/mysql`
* user: admin
* password: admin

### Run ###
* build image: `docker build -t mysql .`
* create container: `docker run -p 3306:3306 -v /host/db/backup/path:/var/lib/mysql:rw --name mysql -d mysql`

### Customizations ###
You can use `MYSQL_POWER_USER`, `MYSQL_POWER_PASS`, `MYSQL_USER`, `MYSQL_PASS` and `MYSQL_DB` env vars to create users and databases:
```
docker run -p 3306:3306 -v /host/db/backup/path:/var/lib/mysql \
    -e MYSQL_POWER_USER="root" \
    -e MYSQL_POWER_PASS="root" \
    -e MYSQL_USER="new_user" \
    -e MYSQL_PASS="new_pass"  \
    -e MYSQL_DB="new_db" \
    --name mysql -d mysql
```

### Default env variables ###
```
ENV MYSQL_POWER_USER root
ENV MYSQL_POWER_PASS root
ENV MYSQL_USER user
ENV MYSQL_PASS pass
ENV MYSQL_DB   test
```
