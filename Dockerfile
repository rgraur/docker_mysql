FROM ubuntu
MAINTAINER  Radu Graur <radu.graur@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# install MySQL server
RUN echo mysql-server mysql-server/root_password password docker | debconf-set-selections; \
  echo mysql-server mysql-server/root_password_again password docker | debconf-set-selections; \
  apt-get update && apt-get install -y mysql-common mysql-server

# remove default databases
RUN rm -rf /var/lib/mysql/*

# add custom config
ADD ./docker.cnf /etc/mysql/conf.d/docker.cnf
RUN chmod 644 "/etc/mysql/conf.d/docker.cnf"

# add startup script
ADD ./start.sh /opt/start.sh
RUN chmod +x /opt/start.sh

# allow configs and databases persistence
VOLUME  ["/etc/mysql", "/var/lib/mysql"]

# default env variables
ENV MYSQL_POWER_USER root
ENV MYSQL_POWER_PASS root
ENV MYSQL_USER user
ENV MYSQL_PASS pass
ENV MYSQL_DB   test

# cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 3306
CMD ["/opt/start.sh"]
