#!/usr/bin/env bash

# start MySQL server
startServer()
{
  # wait for server to start
  /usr/bin/mysqld_safe > /dev/null 2>&1 &
  mysqladmin --silent --wait=30 ping || exit 1
}

# stop MySQL server
stopServer()
{
  killall mysqld && sleep 3s
}

# if uninitialized, install mysql db and create users
if [ ! -f /var/lib/mysql/ibdata1 ]; then
  mysql_install_db > /dev/null 2>&1

  startServer

  # delete all default users
  echo "DELETE FROM mysql.user" | mysql

  # create power user
  echo "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_POWER_USER}'@'%' IDENTIFIED BY '${MYSQL_POWER_PASS}' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql

  # create debian user
  if [ -f /etc/mysql/debian.cnf ]; then
    DEBIAN_PASS=`cat /etc/mysql/debian.cnf  | grep password -m 1 | awk '{print $3}'`
    echo "GRANT RELOAD, SHUTDOWN on *.* TO 'debian-sys-maint'@'localhost' IDENTIFIED BY '${DEBIAN_PASS}'; FLUSH PRIVILEGES" | mysql -u${MYSQL_POWER_USER} -p${MYSQL_POWER_PASS}
  fi

  # create new user and database
  mysqladmin -u${MYSQL_POWER_USER} -p${MYSQL_POWER_PASS} create $MYSQL_DB
  echo "GRANT ALL PRIVILEGES ON ${MYSQL_DB}.* TO '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASS}'; FLUSH PRIVILEGES" | mysql -u${MYSQL_POWER_USER} -p${MYSQL_POWER_PASS}

  stopServer
fi

exec /usr/bin/mysqld_safe
